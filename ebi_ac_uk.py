#https://www.ebi.ac.uk/ols/docs/api
import requests
from ratelimit import limits, RateLimitException, sleep_and_retry

_base_url = "https://www.ebi.ac.uk/ols/api/"

calls_per_minute = 250
def get_calls_per_minute():
	return calls_per_minute

def _url_for_entity(entity):
  return _base_url+entity

def _add_pagination(page=0,per_page = 500):
	return "" if page is -1 else "?page="+str(page)+"&size="+str(per_page)

def url_for_profile():
  return _url_for_entity("profile")

def url_for_ontologies(page=0,per_page = 500):
  return _url_for_entity("ontologies")+_add_pagination(page,per_page)

def url_for_ontology_terms(ontology,page=0,per_page = 500):
  return url_for_ontologies(-1)+"/"+ontology+"/terms"+_add_pagination(page,per_page)

def url_for_ontology_term_relative(ontology,termId,relation):
  return url_for_ontologies(-1)+"/"+ontology+"/"+relation+"?id="+termId

def url_for_individuals(ontology,page=0,per_page = 500):
  return _url_for_entity("individuals")+_add_pagination(page,per_page)

def url_for_terms(page=0,per_page = 500):
  return _url_for_entity("terms")+_add_pagination(page,per_page)

def url_for_properties(page=0,per_page = 500):
  return _url_for_entity("properties")+_add_pagination(page,per_page)

@sleep_and_retry
@limits(calls=calls_per_minute, period=60)
def fetch_data(url):
	res = requests.get(url)
	# todo catch case when status code is not 200
	# print(res.status_code)
	return res.json()

_field_map = {
	'terms' : {
		'label'      : 'label',
		'term_id'    : 'short_form',
		'root'       : 'is_root',
		'references' : 'annotation.database_cross_reference',
		'synonyms'   : 'synonyms',
		'obsolete'   : 'is_obsolete'
	}
}

_index_field = {
	'terms' : 'term_id'
}

_page_map = {
	'pages': 'totalPages',
	'items': 'totalElements',
}

def _extract_page_attribute(data,attribute):
	return data['page'][attribute]

def extract_page_total(data,attribute):
	return _extract_page_attribute(data,_page_map[attribute])

def get_items_from_response(data,data_type):
	return data['_embedded'][data_type]

def create_item_fields(data,data_type):
	item = {}
	for key, response_key in _field_map[data_type].items():
		item[key] = get_index_value(response_key,data)
	return item

def extract_fields_from_data(data,data_type):
	final = {}
	for respItem in data['_embedded'][data_type]:
		item = {}
		for key, response_key in _field_map[data_type].items():
			item[key] = get_index_value(response_key,respItem)
		final[item[_index_field[data_type]]] = item
	return final

def extract_next_page_url_from_data(data):
	return None if not 'next' in data['_links'] else data['_links']['next']['href']

def get_index_value(index,data):
	if not '.' in index:
		return data[index]
	else:
		output = data
		keys = index.split(".")
		for key in keys:
			if key in output:
				output = output[key]
		return output
