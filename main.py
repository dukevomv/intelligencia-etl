from progress.bar import Bar
from datetime import datetime

import settings
from db import insert_bulk
from ebi_ac_uk import url_for_ontology_terms,get_calls_per_minute,create_item_fields,get_items_from_response,extract_page_total,url_for_ontology_term_relative,fetch_data,extract_next_page_url_from_data
import transform

reference_aliases = ['mesh']

settings.init()

settings.print_before_bar('----- ONTOLOGY: '+settings.ontology+' -----')
settings.print_before_bar('Fetching Terms, synonyms and first level parents.')
settings.print_before_bar('API Rate limit set to '+str(get_calls_per_minute())+' calls per minute.')
settings.print_before_bar('Starting with page '+str(settings.starting_page)+ ' of size '+str(settings.page_size))
settings.print_before_bar('Start Time: '+datetime.now().strftime("%H:%M:%S"))

current_url = url_for_ontology_terms(settings.ontology,settings.starting_page,settings.page_size)
while not current_url is None:
	query_data = {
		'terms': [],
		'synonyms': [],
		'references': [],
		'parents': [],
	}
	settings.print_before_bar('Fetching each page for terms...')
	data = fetch_data(current_url)
	total = extract_page_total(data,'items')
	items = get_items_from_response(data,'terms')
	settings.print_before_bar('Looping through each fetched term...')
	for item in items:
		term = create_item_fields(item,'terms')
		query_data['terms'].append(term)
		query_data['synonyms'] = query_data['synonyms'] + transform.synonyms(term)
		query_data['references'] = query_data['references'] + transform.references(term,reference_aliases)
		settings.print_before_bar('- Calculating term synonyms')
		settings.print_before_bar('- Fetching term parents')
		if term['root'] is False:
			parent_url = url_for_ontology_term_relative(settings.ontology,term['term_id'],'parents')
			while not parent_url is None:
				parent_data = fetch_data(parent_url)
				parent_items = get_items_from_response(parent_data,'terms')
				for parent_item in parent_items:
					parent_term = create_item_fields(parent_item,'terms')
					query_data['parents'].append({'term_id':term['term_id'],'parent_id':parent_term['term_id']})
					query_data['terms'].append(parent_term)
					query_data['synonyms'] = query_data['synonyms'] + transform.synonyms(parent_term)
					query_data['references'] = query_data['references'] + transform.references(parent_term,reference_aliases)
				parent_url = extract_next_page_url_from_data(parent_data)

	settings.print_before_bar('Inserting collected terms, synonyms and parent relations...')
	insert_bulk(settings.db,'ontology_term',        ['term_id','label'],                    query_data['terms'],	  ['term_id'])
	insert_bulk(settings.db,'ontology_term_synonym',['term_id','synonym','initial_synonym'],query_data['synonyms'],['term_id','synonym'])
	insert_bulk(settings.db,'ontology_term_parent', ['term_id','parent_id'],                query_data['parents'], ['term_id','parent_id'])
	insert_bulk(settings.db,'ontology_term_reference', ['term_id','ref_alias','ref_id'],    query_data['references'], ['term_id','ref_alias'])

	# time is estimated roughly by the total amount of terms (parent calls) + the calls for iterating all terms divided by the rate limit
	estimated = (total + total/settings.page_size)/get_calls_per_minute()
	#  todo better estimationn would be to calculate first page loop duration and multiply it by remaining pages 

	now = datetime.now()
	current_time = now.strftime("%H:%M:%S")
	current_url = extract_next_page_url_from_data(data)
	settings.print_before_bar('Looping through the rest of the terms...')
	settings.print_before_bar("ETA: "+str(estimated)+" minutes - based on api rate limit for all page results")

	progress_amount = len(items)

	if settings.bar is None:
		settings.bar = Bar('Terms Parsed', max=total)
		progress_amount += settings.starting_page*settings.page_size

	settings.bar.next(progress_amount)

settings.bar.finish()

current_time = datetime.now().strftime("%H:%M:%S")
print("Finish Time: "+current_time)

