CREATE TABLE ontology_term (
	term_id VARCHAR (20) PRIMARY KEY,
	label VARCHAR ( 255 )
);

CREATE TABLE ontology_term_synonym (
	term_id VARCHAR (20),
	synonym VARCHAR (500),
	initial_synonym VARCHAR (500),
	UNIQUE (term_id, synonym),
	CONSTRAINT fk_term
		FOREIGN KEY(term_id) 
		REFERENCES ontology_term(term_id)
		ON DELETE SET NULL
);
-- Initial synonym is a temp column in order to evaluate our transform functions based on the initial synonym

CREATE TABLE ontology_term_parent (
	term_id VARCHAR (20),
	parent_id VARCHAR (20),
	UNIQUE (term_id, parent_id),
	CONSTRAINT fk_parent
		FOREIGN KEY(parent_id) 
		REFERENCES ontology_term(term_id)
		ON DELETE SET NULL,
	CONSTRAINT fk_term
		FOREIGN KEY(term_id) 
		REFERENCES ontology_term(term_id)
		ON DELETE SET NULL
);

CREATE TABLE ontology_term_reference (
	term_id VARCHAR (20),
	ref_alias VARCHAR (128),
	ref_id VARCHAR (128),
	UNIQUE (term_id, ref_alias),
	CONSTRAINT fk_term
		FOREIGN KEY(term_id) 
		REFERENCES ontology_term(term_id)
		ON DELETE SET NULL
);