# IAI Duke

## Pipeline

The pipeline is to fetch Specific ontology terms from API by page
- Create tuples of term_id and label
- Create transform fetched term synonyms to preferred payload
- Create tuples of term_id and synonym
- Check if term is not root
	- Fetch for each term its parents
	- Create the term if not already created
	- Create synonyms for the term
	- Create tuples for parent_id and term_id 
- Create term entries in Database
- Create synonym entries in Database
- Create parent relation entries in Database

The above pipeline continues on next page

## Dependencies

* docker
* docker-compose 3

## Development

To setup project you will need 2 terminals in the project's folder:


### Setup 

The **first** will download docker images and setup environment and database
```
docker-compose up  --build -V
```
Postgres will be available in `5444` port on your local machine with username & password `postgres`.


### Execute 

The **second** terminal will run the main script which will fetch the data.

First get inside the container
```
docker exec -it intelligencia_iai_1 sh
```

Then run `python main.py` to fetch api Data.

The main script requires no attributes but it can adjust the:
- ontology fetched
- starting page 
- page size

example:

```
python main.py --ontology=efo --page=2825 --size=10
```

### Logs

You will find the execution logs in the `main.log` file.