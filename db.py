import psycopg2
import logging

def init_connection(host,db,user="postgres",pswd="postgres",port="5432"):
	return psycopg2.connect(database=db, user=user, password=pswd, host=host, port=port)

def insert_bulk(connection, table, columns, data, conflictColumns = None):
	# create lists with the defined columns only
	values = [[item[c] for c in columns] for item in data]

	cols = ','.join(columns)
	mogrifyColParams = "("+','.join(['%s']*len(columns))+")"

	if len(values) > 0:
		cursor = connection.cursor()
		sqlValues = [cursor.mogrify(mogrifyColParams, v).decode('utf8') for v in values]
		query = "INSERT INTO %s(%s) VALUES " % (table, ','.join(columns)) + ",".join(sqlValues)
		
		if not conflictColumns is None:
			query += " ON CONFLICT ("+','.join(conflictColumns)+") DO NOTHING"
			#https://stackoverflow.com/questions/34514457/bulk-insert-update-if-on-conflict-bulk-upsert-on-postgres

		try:
			cursor.execute(query)
			connection.commit()
		except (Exception, psycopg2.DatabaseError) as error:
			print("\nError: %s" % error)
			logging.error("Error: %s" % error + '\n' +query)
			connection.rollback()
			cursor.close()
			return 1
		cursor.close()
