import logging
import getopt,sys
from db import init_connection

logging.basicConfig(filename='main.log', level=logging.DEBUG,format='%(asctime)s | %(message)s')

def init():
	_init_progress()
	_init_cli_arguments()
	_init_db()

def _init_db():
	global db
	db = init_connection("intelligencia_postgres_1","iai")

def _init_cli_arguments():
	global ontology,starting_page,page_size

	#defaults
	ontology = 'efo'
	starting_page = 0
	page_size = 100

	full_cmd_arguments = sys.argv
	argument_list = full_cmd_arguments[1:]

	short_options = "o:p:s:"
	long_options = ["ontology=","page=","size="]

	try:
		arguments, values = getopt.getopt(argument_list, short_options, long_options)
	except getopt.error as err:
		print (str(err))
		sys.exit(2)

	for current_argument, current_value in arguments:
		if current_argument in ("-o", "--ontology"):
			ontology = current_value
		elif current_argument in ("-p", "--page"):
			starting_page = int(current_value)
		elif current_argument in ("-s", "--size"):
			page_size = int(current_value)

def _init_progress():
	global bar, shownPrints

	bar = None
	shownPrints = []

def print_before_bar(text):
	if bar is None and not text in shownPrints:
		print(text)
		shownPrints.append(text)
