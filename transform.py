import re
import copy
import logging

# -- synonyms --

dont_store = ['InChI']
dont_split = ['disease or disorder','disease and disorder','diseases and disorders']

def _construct_synonym_object(data,synonym):
	obj = copy.deepcopy(data)
	obj['synonym'] = synonym.strip().lower()
	return obj

def _break_term_synonym(termId,synonym):
	data =  {
		'term_id': termId,
		'initial_synonym': synonym,
		'synonym': synonym
	}
	synonyms = []
	#todo Implement here a better transform function to create valid synonym entries
	if len(synonym) > 500:
		logging.error('Ignoring synonym for '+data['term_id']+'\n'+synonym)
	else:
		if not any(ds in synonym for ds in dont_store):
			if any(ds in synonym for ds in dont_split):
				synonyms.append(_construct_synonym_object(data,synonym))
			else:
				arr = re.split(',| and | or ',synonym)
				for i in arr:
					synonyms.append(_construct_synonym_object(data,i))
	return synonyms

def synonyms(term):
	synonyms = []
	if not term['synonyms'] is None:
		for s in term['synonyms']:
			for ss in _break_term_synonym(term['term_id'],s):
				synonyms.append(ss)
	return synonyms




# -- references --

def _construct_reference_object(data,alias,ref_id):
	obj = copy.deepcopy(data)
	obj['ref_alias'] = alias.lower()
	obj['ref_id'] = ref_id
	return obj

def _break_term_reference(termId,references):
	data =  {
		'term_id': termId
	}

	mapped = []
	for r in references:
		arr = re.split(':',r)
		if(len(arr) == 2):
			mapped.append(_construct_reference_object(data,arr[0],arr[1]))
	return mapped

def references(term,acronyms):
	references = []
	if not term['references'] is None:
		calculatedItems = _break_term_reference(term['term_id'],term['references'])
		for i in calculatedItems:
			if i['ref_alias'] in acronyms:
				references.append(i)
	return references

