requests==2.25.1
psycopg2==2.8.6
datetime==4.3
progress==1.5
ratelimit==2.2.1